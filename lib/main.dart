import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:number_trivia/core/network/network_info.dart';
import 'package:number_trivia/core/utils/input_convertor.dart';
import 'package:number_trivia/features/number_trivia/data/datasources/number_trivia_local_data_source.dart';
import 'package:number_trivia/features/number_trivia/data/datasources/number_trivia_remote_data_source.dart';
import 'package:number_trivia/features/number_trivia/data/repositories/number_trivia_repository_impl.dart';
import 'package:number_trivia/features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:number_trivia/features/number_trivia/domain/usecases/get_concrete_number_trivia_usecase.dart';
import 'package:number_trivia/features/number_trivia/presentation/bloc/number_trivia/number_trivia_bloc.dart';
import 'package:number_trivia/features/number_trivia/presentation/pages/number_trivia_page.dart';

import 'features/number_trivia/domain/usecases/get_random_number_trivia_usecase.dart';

void main() async {
  // await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    NumberTriviaRemoteDataSource numberTriviaRemoteDataSource =
        NumberTriviaRemoteDataSourceImpl();
    NumberTriviaLocalDataSource numberTriviaLocalDataSource =
        NumberTriviaLocalDataSourceImpl();
    NetworkInfo networkInfo = NetworkInfoImpl();
    NumberTriviaRepository numberTriviaRepository = NumberTriviaRepositoryImpl(
        remoteDataSource: numberTriviaRemoteDataSource,
        localDataSource: numberTriviaLocalDataSource,
        networkInfo: networkInfo);
    GetConcreteNumberTriviaUseCase getConcreteNumberTriviaUseCase =
        GetConcreteNumberTriviaUseCase(numberTriviaRepository);
    GetRandomNumberTriviaUseCase getRandomNumberTriviaUseCase =
        GetRandomNumberTriviaUseCase(numberTriviaRepository);
    InputConvertor inputConvertor = InputConvertor();

    return BlocProvider(
      create: (context) => NumberTriviaBloc(
          getConcreteNumberTriviaUseCase: getConcreteNumberTriviaUseCase,
          getRandomNumberTriviaUseCase: getRandomNumberTriviaUseCase,
          inputConvertor: inputConvertor),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Number Trivia',
        theme: ThemeData(
            primaryColor: Colors.green.shade800,
            colorScheme: ColorScheme.fromSwatch()
                .copyWith(secondary: Colors.green.shade600)),
        home: const NumberTriviaPage(),
      ),
    );
  }
}
