import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/number_trivia/number_trivia_bloc.dart';

class TriviaControls extends StatefulWidget {
  const TriviaControls({Key? key}) : super(key: key);

  @override
  State<TriviaControls> createState() => _TriviaControlsState();
}

class _TriviaControlsState extends State<TriviaControls> {
  final TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TextField(
          autofocus: true,
          onSubmitted: (_) => _dispatchConcrete(),
          controller: textEditingController,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), hintText: 'Input a border'),
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: <Widget>[
            Expanded(
                child: Container(
              color: Theme.of(context).colorScheme.secondary,
              child: TextButton(
                  onPressed: () => _dispatchConcrete(),
                  child: const Text(
                    "Search",
                    style: TextStyle(color: Colors.white),
                  )),
            )),
            const SizedBox(
              width: 10,
            ),
            Expanded(
                child: Container(
              color: Colors.grey,
              child: TextButton(
                  onPressed: () => _dispatchRandom(),
                  child: const Text(
                    "Get random Trivia",
                    style: TextStyle(color: Colors.white),
                  )),
            )),
          ],
        )
      ],
    );
  }

  void _dispatchConcrete() {
    context.read<NumberTriviaBloc>().add(GetTriviaForConcreteNumberEvent(
        numberString: textEditingController.text));
    textEditingController.clear();
  }

  void _dispatchRandom() {
    context.read<NumberTriviaBloc>().add(GetTriviaForRandomNumberEvent());
    textEditingController.clear();
  }
}
