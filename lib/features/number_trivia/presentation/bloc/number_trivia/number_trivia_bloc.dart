import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:number_trivia/features/number_trivia/domain/entities/number_trivia_entity.dart';
import 'package:number_trivia/features/number_trivia/domain/usecases/get_random_number_trivia_usecase.dart';

import '../../../../../core/usecases/usecase.dart';
import '../../../../../core/utils/input_convertor.dart';
import '../../../domain/usecases/get_concrete_number_trivia_usecase.dart';

part 'number_trivia_event.dart';
part 'number_trivia_state.dart';

class NumberTriviaBloc extends Bloc<NumberTriviaEvent, NumberTriviaState> {
  final GetConcreteNumberTriviaUseCase getConcreteNumberTriviaUseCase;
  final GetRandomNumberTriviaUseCase getRandomNumberTriviaUseCase;
  final InputConvertor inputConvertor;

  NumberTriviaBloc(
      {required this.getConcreteNumberTriviaUseCase,
      required this.getRandomNumberTriviaUseCase,
      required this.inputConvertor})
      : super(NumberTriviaEmptyState()) {
    on<GetTriviaForConcreteNumberEvent>((event, emit) async {
      final inputEither =
          inputConvertor.stringToUnsignedInteger(event.numberString);
      var inputEitherValue = inputEither.fold((l) => l, (r) => r);
      if (inputEither is Left) {
        emit(const NumberTriviaErrorState(
            message: 'Please enter a correct number'));
      } else if (inputEither is Right) {
        emit(NumberTriviaLoadingState());
        final failureOrTrivia = await getConcreteNumberTriviaUseCase(
            Params(number: int.parse(inputEitherValue.toString())));
        var failureOrTriviaValue = failureOrTrivia?.fold((l) => l, (r) => r);
        if (failureOrTrivia is Left) {
          emit(const NumberTriviaErrorState(message: 'Server error'));
        } else if (failureOrTrivia is Right) {
          emit(NumberTriviaLoadedState(
              triviaEntity: failureOrTriviaValue as NumberTriviaEntity));
        }
      }
    });
    on<GetTriviaForRandomNumberEvent>((event, emit) async {
      emit(NumberTriviaLoadingState());
      final failureOrTrivia = await getRandomNumberTriviaUseCase(NoParams());
      var failureOrTriviaValue = failureOrTrivia?.fold((l) => l, (r) => r);
      if (failureOrTrivia is Left) {
        emit(const NumberTriviaErrorState(message: 'Server error'));
      } else if (failureOrTrivia is Right) {
        emit(NumberTriviaLoadedState(
            triviaEntity: failureOrTriviaValue as NumberTriviaEntity));
      }
    });
  }
}
