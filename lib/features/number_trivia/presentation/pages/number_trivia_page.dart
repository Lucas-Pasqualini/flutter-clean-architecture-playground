import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:number_trivia/features/number_trivia/domain/entities/number_trivia_entity.dart';
import 'package:number_trivia/features/number_trivia/presentation/bloc/number_trivia/number_trivia_bloc.dart';

import '../widgets/loading_widget.dart';
import '../widgets/message_display_widget.dart';
import '../widgets/trivia_controls_widget.dart';
import '../widgets/trivia_display_widget.dart';

class NumberTriviaPage extends StatelessWidget {
  const NumberTriviaPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Number Trivia'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                const SizedBox(height: 10),
                BlocBuilder<NumberTriviaBloc, NumberTriviaState>(
                  builder: (context, state) {
                    if (state is NumberTriviaEmptyState) {
                      return const MessageDisplay(
                        message: 'Start searching!',
                      );
                    } else if (state is NumberTriviaLoadingState) {
                      return const LoadingWidget();
                    } else if (state is NumberTriviaLoadedState) {
                      return TriviaDisplayWidget(
                        numberTriviaEntity: NumberTriviaEntity(
                            text: state.triviaEntity.text,
                            number: state.triviaEntity.number),
                      );
                    } else if (state is NumberTriviaErrorState) {
                      print(state.message);
                      return MessageDisplay(message: state.message);
                    }
                    return const Text('');
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                const TriviaControls(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
