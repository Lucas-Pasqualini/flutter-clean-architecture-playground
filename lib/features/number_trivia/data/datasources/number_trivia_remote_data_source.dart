import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:number_trivia/core/error/exceptions.dart';
import 'package:number_trivia/features/number_trivia/data/models/number_trivia_model.dart';

abstract class NumberTriviaRemoteDataSource {
  Future<NumberTriviaModel>? getConcreteNumberTrivia(int? number);
  Future<NumberTriviaModel>? getRandomNumberTrivia();
}

class NumberTriviaRemoteDataSourceImpl implements NumberTriviaRemoteDataSource {
  @override
  Future<NumberTriviaModel>? getConcreteNumberTrivia(int? number) =>
      _getTriviaFormUrl('numbersapi.com', '$number');

  @override
  Future<NumberTriviaModel>? getRandomNumberTrivia() =>
      _getTriviaFormUrl('numbersapi.com/random', '');

  Future<NumberTriviaModel> _getTriviaFormUrl(String url, String number) async {
    var uri = Uri.parse('http://$url/$number?json');
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      return NumberTriviaModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
