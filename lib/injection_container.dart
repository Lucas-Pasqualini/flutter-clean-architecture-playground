import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:number_trivia/core/network/network_info.dart';
import 'package:number_trivia/features/number_trivia/data/datasources/number_trivia_local_data_source.dart';
import 'package:number_trivia/features/number_trivia/data/datasources/number_trivia_remote_data_source.dart';
import 'package:number_trivia/features/number_trivia/data/repositories/number_trivia_repository_impl.dart';
import 'package:number_trivia/features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:number_trivia/features/number_trivia/domain/usecases/get_concrete_number_trivia_usecase.dart';
import 'package:number_trivia/features/number_trivia/domain/usecases/get_random_number_trivia_usecase.dart';
import 'package:number_trivia/features/number_trivia/presentation/bloc/number_trivia/number_trivia_bloc.dart';

import 'core/utils/input_convertor.dart';

final get = GetIt.instance;

Future<void> init() async {
  initBloc();
  initUseCase();
  initRepositories();
  initDataSources();
  initCore();
  initExternal();
}

void initBloc() {
  get.registerFactory(() => NumberTriviaBloc(
      getConcreteNumberTriviaUseCase: get(),
      getRandomNumberTriviaUseCase: get(),
      inputConvertor: get()));
}

void initUseCase() {
  get.registerLazySingleton(() => GetConcreteNumberTriviaUseCase(get()));
  get.registerLazySingleton(() => GetRandomNumberTriviaUseCase(get()));
}

void initRepositories() {
  get.registerLazySingleton<NumberTriviaRepository>(() =>
      NumberTriviaRepositoryImpl(
          remoteDataSource: get(), localDataSource: get(), networkInfo: get()));
}

void initDataSources() {
  get.registerLazySingleton<NumberTriviaRemoteDataSource>(
      () => NumberTriviaRemoteDataSourceImpl());

  get.registerLazySingleton<NumberTriviaLocalDataSource>(
      () => NumberTriviaLocalDataSourceImpl());
}

void initCore() {
  get.registerLazySingleton(() => InputConvertor());
  get.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl());
}

void initExternal() {
  get.registerLazySingleton(() => http.Client());
}
