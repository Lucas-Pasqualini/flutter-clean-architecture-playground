import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../features/number_trivia/domain/entities/number_trivia_entity.dart';
import '../error/failure.dart';

abstract class UseCase<Type, Params> {
  Future<Either<Failure, NumberTriviaEntity>?> call(Params params);
}

class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}
